<!DOCTYPE html>
<html lang="en">

<head>
    <!-- <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
    <title>Document</title>
</head>

<body>
    <div>
        <h1>Buat Account Baru!</h1>
        <h2>Sign Up Form</h2>
    </div>

    <form action="/welcome" method="post">
        @csrf
        <label for="first_name">First name:</label><br><br>
        <input type="text" name="first_name" id="first_name"><br><br>
        <label for="last_name">Last name:</label><br><br>
        <input type="text" name="last_name" id="last_name"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender" id="" value="Male">Male<br>
        <input type="radio" name="gender" id="" value="Female">Female<br>
        <input type="radio" name="gender" id="" value="Other">Other<br><br>
        <label>Nationality:</label><br><br>
        <select name="nationality" id="">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select>
        <br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="language" id="" value="Bahasa Indonesia">Bahasa Indonesia<br>
        <input type="checkbox" name="language" id="" value="English">English<br>
        <input type="checkbox" name="language" id="" value="Other">Other<br><br>

        <label for="bio">Bio:</label><br><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
</body>

</html>