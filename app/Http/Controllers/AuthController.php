<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index()
    {
        return view('register');
    }

    public function register(Request $request)
    {
        $firstName = $request->first_name;
        $lastName = $request->last_name;
        return view('welcome', compact('firstName', 'lastName'));
    }
}
